#!/usr/bin/env python
# --coding: utf-8 --
from datetime import datetime
from pytz import timezone

import logging
import tornado.escape
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.websocket
import os.path
import uuid

from tornado.options import define, options
define("port", default=8888, help="run on the given port", type=int)

time_zone = timezone('Europe/Moscow')

class Application(tornado.web.Application):
    def __init__(self):

        # Тут вешают обработчики на урлы
        handlers = [
            (r"/", MainHandler),
            (r"/chatsocket", SocketHandler),
        ]

        # Тут что-то неважное на данный момент происходит
        settings = dict(
            cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            xsrf_cookies=True,
        )
        super(Application, self).__init__(handlers, **settings)

#Этот класс рэндрит темплейт на GET запрос тут вот видишь (r"/", MainHandler),
class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("index.html", messages=SocketHandler.cache)


# Это класс сокета инстациируется при запуске сервера, тобишь chatdemo.py
class SocketHandler(tornado.websocket.WebSocketHandler):
    # Это список подключений
    waiters = set()
    cache = []
    cache_size = 200

    def get_compression_options(self):
        # Non-None enables compression with default options.
        return {}

    # Этот метод срабатывает на новое подключение
    def open(self):

        # Добавляем себе id
        self.id = str(uuid.uuid4())
        
        # Добавляем себя в список подключений 
        SocketHandler.waiters.add(self)

    def on_close(self):
        SocketHandler.waiters.remove(self)

    @classmethod
    def update_cache(self, chat):
        self.cache.append(chat)
        if len(self.cache) > self.cache_size:
            self.cache = self.cache[-self.cache_size:]


    @classmethod
    def send_updates(self, chat):
        #logging.info("sending message to %d waiters", len(self.waiters))
        for waiter in self.waiters:
            try:
                # Тут короче попытка адресации сообщений, она даже работает немного
                # но кэш пока не адресный, а им все пользуются, 
                # так что адресные сообщения таки лучше отдельным функцоналом мутить
                # upd ща подправил чтобы в кэш только с to=0 лезло но это обрубок
                # и сбоит что-то, ща в двух браузерах идут адресные, в трий не приходят)
                if chat["to"] == 0 or chat["to"] == waiter.id:
                    waiter.write_message(chat)
            except:
                logging.error("Error sending message", exc_info=True)

    def on_message(self, message):
        logging.info("got message %r", message)
        parsed = tornado.escape.json_decode(message)
        if not parsed["to"]: 
            to = 0
        else:
            to =  parsed["to"]#если 0 то всем, [id, id, id]
        
        chat = {
            "to": to,
            "id": str(uuid.uuid4()),
            "body": parsed["body"],
            "datetime": datetime.now(time_zone).strftime("%Y-%m-%d %H:%M:%S"),
            "uid": self.id,
            }

        chat["html"] = tornado.escape.to_basestring(
            self.render_string("message.html", message=chat))
        
        if chat["to"] == 0:
            SocketHandler.update_cache(chat)
        
        SocketHandler.send_updates(chat)        


def main():
    tornado.options.parse_command_line()
    app = Application()
    app.listen(options.port)
    tornado.ioloop.IOLoop.current().start()

if __name__ == "__main__":
    main()
